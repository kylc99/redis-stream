package com.duykypaul.redisstreamproducer.producer;

import com.duykypaul.redisstreamproducer.config.ConfigProperties;
import com.duykypaul.redisstreamproducer.dto.Category;
import com.duykypaul.redisstreamproducer.dto.Product;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.connection.stream.StreamRecords;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Slf4j
public class PurchaseEventProducer {

    private final AtomicInteger atomicInteger = new AtomicInteger(0);

    @Autowired
    private ConfigProperties config;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Scheduled(fixedRate = 1000000)
    public void publishEvent() throws UnknownHostException {
        handlePublish(1);
    }

    @Scheduled(fixedRate = 10000)
    public void showPublishedEventsSoFar(){
        log.info( "Total Events :: " + atomicInteger.get());
    }

    @SneakyThrows
    public void handlePublish(int n) {
        for (int i = 0; i < n; i++) {
            Product product = new Product(atomicInteger.incrementAndGet(), "heater", 65.00, Category.APPLIANCES);

            String json = new Gson().toJson(product);
            ObjectRecord<String, String> recordT = StreamRecords.newRecord()
                    .ofObject(json)
                    .withStreamKey(config.getStream("purchase-events"));
            this.redisTemplate
                    .opsForStream()
                    .add(recordT);
            log.info(InetAddress.getLocalHost().getHostName() + " - produced :" + json);
        }
    }

}
