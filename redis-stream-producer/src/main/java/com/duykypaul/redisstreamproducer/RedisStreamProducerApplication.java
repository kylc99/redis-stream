package com.duykypaul.redisstreamproducer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class RedisStreamProducerApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(RedisStreamProducerApplication.class, args);

    }

    @Override
    public void run(String... args) throws Exception {

    }
}
