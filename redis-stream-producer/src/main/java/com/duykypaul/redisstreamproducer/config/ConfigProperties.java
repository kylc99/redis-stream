package com.duykypaul.redisstreamproducer.config;

import lombok.Data;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "")
@EnableAutoConfiguration
@Data
public class ConfigProperties {
    private String keyPrefix;
    private String keySeparator;
    private String keyNotifications;
    private long streamPollTimeout = 100;

    private String serviceKey;

    private String consumerGroupName;

    /**
     * Generate a key using all string and key separator
     * @return
     */
    public String getCompleteKeyName(String... keys){
        return String.join(keySeparator, keys);
    }


    public String getStream(String keyStream){
        return getCompleteKeyName(keyPrefix, keyStream);
    }

}
