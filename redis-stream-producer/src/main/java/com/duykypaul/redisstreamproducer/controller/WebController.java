package com.duykypaul.redisstreamproducer.controller;

import com.duykypaul.redisstreamproducer.producer.PurchaseEventProducer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(path = "/api")
@RequiredArgsConstructor
public class WebController {

    private final PurchaseEventProducer purchaseEventProducer;
//    private final CloseAppProducer closeAppProducer;
    @GetMapping("/purchase/{amount}")
    public String purchase(@PathVariable int amount){
        purchaseEventProducer.handlePublish(amount);
        return "OK";
    }

    @GetMapping("/close-app/{amount}")
    public String closeApp(@PathVariable int amount){
//        closeAppProducer.handlePublish(amount);
        return "OK";
    }
}
