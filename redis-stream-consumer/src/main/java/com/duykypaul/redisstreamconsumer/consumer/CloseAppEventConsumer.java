//package com.duykypaul.redisstreamconsumer.consumer;
//
//
//import com.duykypaul.redisstreamconsumer.config.Config;
//import com.duykypaul.redisstreamconsumer.dto.Product;
//import com.google.gson.Gson;
//import io.lettuce.core.api.async.RedisAsyncCommands;
//import io.lettuce.core.codec.StringCodec;
//import io.lettuce.core.output.StatusOutput;
//import io.lettuce.core.protocol.CommandArgs;
//import io.lettuce.core.protocol.CommandKeyword;
//import io.lettuce.core.protocol.CommandType;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.DisposableBean;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Range;
//import org.springframework.data.redis.connection.stream.*;
//import org.springframework.data.redis.core.StringRedisTemplate;
//import org.springframework.data.redis.stream.StreamListener;
//import org.springframework.data.redis.stream.StreamMessageListenerContainer;
//import org.springframework.data.redis.stream.Subscription;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Service;
//
//import java.net.InetAddress;
//import java.time.Duration;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Objects;
//import java.util.concurrent.atomic.AtomicInteger;
//
//@Service
//@Slf4j
//public class CloseAppEventConsumer implements InitializingBean, DisposableBean, StreamListener<String, ObjectRecord<String, String>> {
//
//    private final AtomicInteger consumed = new AtomicInteger(0);
//    private final AtomicInteger reConsumed = new AtomicInteger(0);
//    private final AtomicInteger consumptionFailedAgain = new AtomicInteger(0);
//
//    @Autowired
//    private Config config;
//
//    @Autowired
//    private StringRedisTemplate template;
//
//    private StreamMessageListenerContainer<String, ObjectRecord<String, String>> container;
//    private Subscription subscription;
//    private String streamName;
//    private String consumerGroupName;
//    private String consumerName;
//    private String retryConfigurationKey;
//    private String serviceKey;
//
//    @Override
//    public void onMessage(ObjectRecord<String, String> message) {
//        try {
//            Product product = new Gson().fromJson(message.getValue(), Product.class);
//            if (product.getId() % 9 == 0) {
//                throw new Exception("random handle message error");
//            }
//            //danh dau record da xu ly
//            template.opsForStream().acknowledge(streamName, consumerGroupName, message.getId());
//            template.opsForStream().delete(streamName, message.getId());
//            consumed.incrementAndGet();
//            log.info(InetAddress.getLocalHost().getHostName() + " - consumed :" + message.getValue());
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//            template.convertAndSend(config.getNotificationChannel(),
//                    String.format("Error in consumer service, Cannot handle with messageId(%s)", message.getId()));
//            template.opsForHash().increment(config.getServiceKey(), "errors", 1);
//            log.info("Error in consumer service, Cannot handle with messageId({}), messageValue({})", message.getId(), message.getValue());
//        }
//    }
//
//    @Scheduled(fixedRate = 5000)
//    public void processPendingMessages() throws InterruptedException {
//        Map retryConfiguration = getRetryConfiguration();
//        // if the current consumer is the one to process pending messages start processing
//        if (retryConfiguration.get(Config.CONFIG_CONSUMER_NAME).toString().equalsIgnoreCase(consumerName)) {
//            long nbOfMessageToFetch = Long.parseLong(retryConfiguration.get(Config.CONFIG_MAX_CLAIMED_MESSAGES).toString());
//            PendingMessages messages = template.opsForStream().pending(streamName, consumerGroupName, Range.unbounded(), nbOfMessageToFetch);
//            if (!messages.isEmpty()) {
//                log.info("Processing pending message in " + consumerName + " consumer");
//            }
//            for (PendingMessage message : messages) {
//                RedisAsyncCommands commands = (RedisAsyncCommands) template.getConnectionFactory().getConnection().getNativeConnection();
//                CommandArgs<String, String> args = new CommandArgs<>(StringCodec.UTF8)
//                        .add(streamName)
//                        .add(consumerGroupName)
//                        .add(consumerName)
//                        .add("10")
//                        .add(message.getIdAsString());
//                commands.dispatch(CommandType.XCLAIM, new StatusOutput<>(StringCodec.UTF8), args);
//                log.info("Message " + message.getIdAsString() + " claimed by " + consumerGroupName + ":" + consumerName);
//
//                // if the number of retry is bigger than configuration acknowledge it
//                // TODO : create a list of failed messages
//                log.info("message.getTotalDeliveryCount with id({}), count({})", message.getId(), message.getTotalDeliveryCount());
//                if (message.getTotalDeliveryCount() > Long.parseLong(retryConfiguration.get(Config.CONFIG_MAX_RETRIES).toString()) - 1) {
//                    template.opsForStream().acknowledge(streamName, consumerGroupName, message.getIdAsString());
//                    log.info(" ack sent for " + message.getIdAsString());
//                    // if configured to be deleted let's delete the message
//                    if (retryConfiguration.get(Config.CONFIG_DELETE_ON_ERROR).toString().equalsIgnoreCase("true")) {
//                        template.opsForStream().delete(streamName, message.getIdAsString());
//                        log.info(" deleted message " + message.getIdAsString());
//                    }
//                    consumptionFailedAgain.incrementAndGet();
//                } else { // process message
//                    // use the string converter to try to convert the number and doo the operation
//                    List<ObjectRecord<String, String>> messagesToProcess = template.opsForStream()
//                            .range(String.class, streamName, Range.closed(message.getIdAsString(), message.getIdAsString()));
//                    if (messagesToProcess.isEmpty()) {
//                        log.error("Message is not present. It has probably been deleted by another process : " + message.getIdAsString());
//                    } else {
//                        ObjectRecord<String, String> msg = messagesToProcess.get(0);
//                        try {
//                            Product product = new Gson().fromJson(msg.getValue(), Product.class);
//                            if (product.getId() % 54 == 0) {
//                                throw new Exception("random retry handle message error");
//                            }
//                            log.info(consumerName + " - retry consumed :" + msg.getValue());
//
//                            template.opsForHash().increment(serviceKey, "processed_from_retry", 1);
//
//                            template.opsForStream().acknowledge(consumerGroupName, msg);
//                            log.info("message processed, from retry method " + msg.getId());
//                            template.opsForStream().delete(streamName, message.getIdAsString());
//                            reConsumed.incrementAndGet();
//
//                        } catch (Exception e) {
//                            template.convertAndSend(config.getNotificationChannel(),
//                                    String.format("Error in consumer service, Cannot retry handle with messageId(%s)", msg.getId()));
//                            template.opsForHash().increment(serviceKey, "errors", 1);
//                            log.info("Error in consumer service, Cannot retry handle with messageId({}), messageValue({})", msg.getId(), msg.getValue());
//                        }
//                    }
//                }
//            }
//
//        } else {
//            log.info("The consumer({}) is not configured to process pending messages", consumerName);
//        }
//
//    }
//
//    @Scheduled(fixedRate = 10000)
//    public void showPublishedEventsSoFar() {
//        log.info("Total Consumed :: " + consumed.get());
//        if (reConsumed.get() > 0) {
//            log.info("Total re-Consumed :: " + reConsumed.get());
//        }
//
//        if (consumptionFailedAgain.get() > 0) {
//            log.info("Total retry fail :: " + consumptionFailedAgain.get());
//        }
//
//    }
//
//    @Override
//    public void afterPropertiesSet() throws Exception {
//        streamName = config.getStream("close-app");
//        consumerGroupName = config.getConsumerGroup("close-app");
//        retryConfigurationKey = config.getRetryConfigKey(consumerGroupName);
//        consumerName = InetAddress.getLocalHost().getHostName();
//        serviceKey = config.getServiceKey();
//        initEventStreamProperties(streamName, consumerGroupName);
//
//        var options = StreamMessageListenerContainer
//                .StreamMessageListenerContainerOptions
//                .builder()
//                .pollTimeout(Duration.ofSeconds(1))
//                .targetType(String.class)
//                .build();
//        this.container = StreamMessageListenerContainer
//                .create(Objects.requireNonNull(template.getConnectionFactory()), options);
//
//        this.subscription = container.receive(
//                Consumer.from(consumerGroupName, consumerName),
//                StreamOffset.create(streamName, ReadOffset.lastConsumed()),
//                this);
//
//        subscription.await(Duration.ofSeconds(2));
//        container.start();
//    }
//
//    @Override
//    public void destroy() throws Exception {
//        if (subscription != null) {
//            subscription.cancel();
//        }
//        if (container != null) {
//            container.stop();
//        }
//    }
//
//
//    /**
//     * khoi tao event stream
//     *
//     * @param eventName
//     * @param groupConsumerName
//     */
//    private void initEventStreamProperties(String eventName, String groupConsumerName) {
//        try {
//            if (Boolean.FALSE.equals(template.hasKey(eventName))) {
//                createNewStreamIfNotExist(eventName, groupConsumerName);
//            } else {
//                createConsumerGroup(eventName, groupConsumerName);
//            }
//        } catch (Exception e) {
//            log.error("Consumer group already present: {}", groupConsumerName);
//        }
//    }
//
//    /**
//     * tao moi consumer group
//     *
//     * @param eventName
//     * @param groupConsumerName
//     */
//    private void createConsumerGroup(String eventName, String groupConsumerName) {
//        log.info("Create {} consumer group", groupConsumerName);
//        template.opsForStream().createGroup(eventName, ReadOffset.from("0"), groupConsumerName);
//    }
//
//    /**
//     * tao moi stream neu chua ton tai
//     *
//     * @param eventName
//     * @param groupConsumerName
//     */
//    private void createNewStreamIfNotExist(String eventName, String groupConsumerName) {
//        log.info("{} does not exist. Creating stream along with the consumer group", eventName);
//        RedisAsyncCommands<String, String> commands = (RedisAsyncCommands<String, String>) Objects.requireNonNull(
//                template.getConnectionFactory()).getConnection().getNativeConnection();
//        CommandArgs<String, String> args = new CommandArgs<>(StringCodec.UTF8)
//                .add(CommandKeyword.CREATE)
//                .add(eventName)
//                .add(groupConsumerName)
//                .add("0")
//                .add("MKSTREAM");
//        commands.dispatch(CommandType.XGROUP, new StatusOutput<>(StringCodec.UTF8), args);
//
//    }
//
//    private Map getRetryConfiguration() {
//        Map config = template.opsForHash().entries(retryConfigurationKey);
//        if (config == null || config.isEmpty()) { // create the key
//            config = new HashMap();
//            config.put(Config.CONFIG_CONSUMER_NAME, consumerName);
//            config.put(Config.CONFIG_MAX_RETRIES, "5");
//            config.put(Config.CONFIG_DELETE_ON_ERROR, "false");
//            config.put(Config.CONFIG_MAX_CLAIMED_MESSAGES, "100");
//
//            template.opsForHash().putAll(retryConfigurationKey, config);
//        }
//        return config;
//    }
//
//}