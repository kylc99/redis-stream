package com.duykypaul.redisstreamconsumer.dto;

public enum Category {
    APPLIANCES,
    BOOKS,
    COSMETICS,
    ELECTRONICS,
    OUTDOOR;
}
