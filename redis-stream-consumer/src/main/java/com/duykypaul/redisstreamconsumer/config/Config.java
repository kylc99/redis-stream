package com.duykypaul.redisstreamconsumer.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "")
@EnableAutoConfiguration
@Data
@Slf4j
public class Config {

    // Name of the fields of the configuration
    public static final String CONFIG_CONSUMER_NAME = "consumer_name";
    public static final String CONFIG_MAX_RETRIES = "max_retries";
    public static final String CONFIG_DELETE_ON_ERROR = "delete_on_error";
    public static final String CONFIG_MAX_CLAIMED_MESSAGES = "max_claimed_messages";

    private String keyPrefix;
    private String keySeparator;
    private String keyNotifications;
    private long streamPollTimeout = 500;

    private String serviceKey;
//    private String keyStream;
//    private String consumerGroupName;

    /**
     * Generate a key using all string and keyseparator
     *
     * @return
     */
    public String getCompleteKeyName(String... keys) {
        return String.join(keySeparator, keys);
    }


    public String getStream(String keyStream) {
        return getCompleteKeyName(keyPrefix, keyStream + "-events");
    }

    public String getConsumerGroup(String keyStream) {
        return keyStream + "-consumer";
    }

    public String getNotificationChannel() {
        return getCompleteKeyName(keyPrefix, keyNotifications);
    }

    public String getServiceKey() {
        return getCompleteKeyName(keyPrefix, "service", serviceKey);
    }

    public String getRetryConfigKey(String consumerGroupName) {
        return getCompleteKeyName(keyPrefix, "retry", consumerGroupName);
    }
}
