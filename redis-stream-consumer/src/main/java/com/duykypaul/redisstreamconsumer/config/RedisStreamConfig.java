//package com.duykypaul.redisstreamconsumer.config;
//
//import com.duykypaul.redisstreamconsumer.consumer.PurchaseEventConsumer;
//import io.lettuce.core.api.async.RedisAsyncCommands;
//import io.lettuce.core.codec.StringCodec;
//import io.lettuce.core.output.StatusOutput;
//import io.lettuce.core.protocol.CommandArgs;
//import io.lettuce.core.protocol.CommandKeyword;
//import io.lettuce.core.protocol.CommandType;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.connection.RedisConnectionFactory;
//import org.springframework.data.redis.connection.stream.Consumer;
//import org.springframework.data.redis.connection.stream.ReadOffset;
//import org.springframework.data.redis.connection.stream.StreamOffset;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.stream.StreamMessageListenerContainer;
//import org.springframework.data.redis.stream.Subscription;
//
//import java.net.InetAddress;
//import java.net.UnknownHostException;
//import java.time.Duration;
//import java.util.Objects;
//
//@Configuration
//@RequiredArgsConstructor
//@Slf4j
//public class RedisStreamConfig {
//
//    @Value("${stream.key}")
//    private String streamKey;
//
//    private final RedisTemplate<String, String> redisTemplate;
//
//    @Bean
//    public Subscription subscription(RedisConnectionFactory redisConnectionFactory,
//                                     PurchaseEventConsumer streamListener) throws UnknownHostException {
//        initEventStreamProperties(streamKey, streamKey);
//
//        var options = StreamMessageListenerContainer
//                .StreamMessageListenerContainerOptions
//                .builder()
//                .pollTimeout(Duration.ofSeconds(1))
//                .targetType(String.class)
//                .build();
//        var listenerContainer = StreamMessageListenerContainer
//                .create(redisConnectionFactory, options);
//
//        var subscription = listenerContainer.receive(
//                Consumer.from(streamKey, InetAddress.getLocalHost().getHostName()),
//                StreamOffset.create(streamKey, ReadOffset.lastConsumed()),
//                streamListener);
//        listenerContainer.start();
//        return subscription;
//    }
//
//    /**
//     * khoi tao event stream
//     *
//     * @param eventName
//     * @param groupConsumerName
//     */
//    private void initEventStreamProperties(String eventName, String groupConsumerName) {
//        try {
//            if (Boolean.FALSE.equals(redisTemplate.hasKey(eventName))) {
//                createNewStreamIfNotExist(eventName, groupConsumerName);
//            } else {
//                createConsumerGroup(eventName, groupConsumerName);
//            }
//        } catch (Exception e) {
//            log.error("Consumer group already present: {}", groupConsumerName);
//        }
//    }
//
//    /**
//     * tao moi consumer group
//     *
//     * @param eventName
//     * @param groupConsumerName
//     */
//    private void createConsumerGroup(String eventName, String groupConsumerName) {
//        log.info("Create {} consumer group", groupConsumerName);
//        redisTemplate.opsForStream().createGroup(eventName, ReadOffset.from("0"), groupConsumerName);
//    }
//
//    /**
//     * tao moi stream neu chua ton tai
//     *
//     * @param eventName
//     * @param groupConsumerName
//     */
//    private void createNewStreamIfNotExist(String eventName, String groupConsumerName) {
//        log.info("{} does not exist. Creating stream along with the consumer group", eventName);
//        RedisAsyncCommands<String, String> commands = (RedisAsyncCommands<String, String>) Objects.requireNonNull(
//                redisTemplate.getConnectionFactory()).getConnection().getNativeConnection();
//        CommandArgs<String, String> args = new CommandArgs<>(StringCodec.UTF8)
//                .add(CommandKeyword.CREATE).add(eventName).add(groupConsumerName).add("0").add("MKSTREAM");
//        commands.dispatch(CommandType.XGROUP, new StatusOutput<>(StringCodec.UTF8), args);
//
//    }
//}
